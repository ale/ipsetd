// This server is heavily based on the reference RAFT example at
// https://github.com/goraft/raftd.

package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"path/filepath"
	"strings"
	"sync"

	"git.autistici.org/ale/ipsetd/client"
	"git.autistici.org/ale/ipsetd/command"
	"git.autistici.org/ale/ipsetd/ipset"
	"github.com/goraft/raft"
	"github.com/gorilla/mux"
)

type Server struct {
	name       string
	path       string
	host       string
	port       int
	router     *mux.Router
	raftServer raft.Server
	httpServer *http.Server
	mutex      sync.RWMutex
	ipset      *ipset.IPSet
}

// Create a new HTTP server supporting the RAFT protocol at
// 'host:port'. RAFT logs are saved in 'path'. Use a different path
// for every server instance.
func NewServer(path, host string, port int) *Server {
	s := &Server{
		name:   host,
		host:   host,
		port:   port,
		path:   path,
		router: mux.NewRouter(),
		ipset:  ipset.NewIPSet(),
	}

	// Autogenerate a name for every instance. Save it in a file
	// in 'path'.
	if b, err := ioutil.ReadFile(filepath.Join(path, "name")); err == nil {
		s.name = strings.TrimSpace(string(b))
	} else {
		s.name = fmt.Sprintf("%s-%07x", host, rand.Int())
		if err := ioutil.WriteFile(filepath.Join(path, "name"), []byte(s.name), 0644); err != nil {
			log.Printf("Error saving node name: %s", err)
		}
	}

	return s
}

func (s *Server) connectionString() string {
	return fmt.Sprintf("http://%s:%d", s.host, s.port)
}

func (s *Server) ListenAndServe(peer string) error {
	var err error

	// Create the RAFT HTTP transport.
	transporter := raft.NewHTTPTransporter("/raft")
	s.raftServer, err = raft.NewServer(s.name, s.path, transporter, s.ipset, s.ipset, "")
	if err != nil {
		return err
	}
	transporter.Install(s.raftServer, s)
	s.raftServer.Start()

	// Initialize the RAFT cluster.
	if s.raftServer.IsLogEmpty() {
		if peer != "" {
			log.Printf("Attempting to join peer: %s", peer)
			if err := s.Join(peer); err != nil {
				return err
			}
		} else {
			log.Println("Initializing new cluster")

			_, err := s.raftServer.Do(
				&raft.DefaultJoinCommand{
					Name:             s.raftServer.Name(),
					ConnectionString: s.connectionString(),
				})
			if err != nil {
				return err
			}
		}
	} else {
		if peer != "" {
			log.Println("Warning: log is not empty, ignoring --peer option")
		}
		log.Println("Recovering from log")
	}

	// Initialize the HTTP server.
	s.httpServer = &http.Server{
		Addr:    fmt.Sprintf(":%d", s.port),
		Handler: s.router,
	}

	s.router.HandleFunc("/join", s.joinHandler).Methods("POST")
	s.router.HandleFunc("/api/1/ipset/create", s.createHandler).Methods("POST")
	s.router.HandleFunc("/api/1/ipset/add", s.addHandler).Methods("POST")
	s.router.HandleFunc("/api/1/ipset/del", s.delHandler).Methods("POST")

	log.Printf("HTTP server listening at %s", s.connectionString())

	return s.httpServer.ListenAndServe()
}

// This is a hack around Gorilla mux not providing the correct
// net/http HandleFunc() interface.
func (s *Server) HandleFunc(pattern string, handler func(http.ResponseWriter, *http.Request)) {
	s.router.HandleFunc(pattern, handler)
}

// Join the current node to an existing cluster.
func (s *Server) Join(peer string) error {
	cmd := &raft.DefaultJoinCommand{
		Name:             s.raftServer.Name(),
		ConnectionString: s.connectionString(),
	}

	var b bytes.Buffer
	json.NewEncoder(&b).Encode(cmd)

	// Use our own 'smart' HTTP client to redirect to the leader
	// if necessary.
	httpClient := client.NewSmartHTTPClient([]string{peer})
	return httpClient.Post("/join", "application/json", &b)
}

func (s *Server) handleError(w http.ResponseWriter, err error) {
	if err == raft.NotLeaderError {
		// Provide a hint to the client with the current leader.
		w.Header().Set("X-Leader", s.raftServer.Leader())
		http.Error(w, err.Error(), http.StatusConflict)
	} else {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
}

func (s *Server) joinHandler(w http.ResponseWriter, r *http.Request) {
	var cmd raft.DefaultJoinCommand
	if err := json.NewDecoder(r.Body).Decode(&cmd); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if _, err := s.raftServer.Do(&cmd); err != nil {
		s.handleError(w, err)
	}
}

func (s *Server) addHandler(w http.ResponseWriter, r *http.Request) {
	var cmd command.AddCommand
	if err := json.NewDecoder(r.Body).Decode(&cmd); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if cmd.SetName == "" || cmd.Entry == "" {
		http.Error(w, "Empty set/entry", http.StatusBadRequest)
		return
	}

	if _, err := s.raftServer.Do(&cmd); err != nil {
		s.handleError(w, err)
	}
}

func (s *Server) delHandler(w http.ResponseWriter, r *http.Request) {
	var cmd command.DelCommand
	if err := json.NewDecoder(r.Body).Decode(&cmd); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if cmd.SetName == "" || cmd.Entry == "" {
		http.Error(w, "Empty set/entry", http.StatusBadRequest)
		return
	}

	if _, err := s.raftServer.Do(&cmd); err != nil {
		s.handleError(w, err)
	}
}

func (s *Server) createHandler(w http.ResponseWriter, r *http.Request) {
	var cmd command.CreateCommand
	if err := json.NewDecoder(r.Body).Decode(&cmd); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if cmd.SetName == "" || cmd.TypeName == "" {
		http.Error(w, "Empty set/type", http.StatusBadRequest)
		return
	}

	if _, err := s.raftServer.Do(&cmd); err != nil {
		s.handleError(w, err)
	}
}

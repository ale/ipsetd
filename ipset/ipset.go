package ipset

import (
	"bytes"
	"encoding/json"
	"log"
	"os/exec"
	"strings"
)

var ipsetBinary = "/usr/sbin/ipset"

// Interface to 'ipset' on the local system. We keep track of the sets
// that are managed via ipsetd, so that we can know which ones to
// dump/restore. IPSet satisfies the RAFT StateMachine interface.
type IPSet struct {
	knownSets map[string]struct{}
}

func NewIPSet() *IPSet {
	return &IPSet{
		knownSets: make(map[string]struct{}),
	}
}

// Run 'ipset' with the specified arguments. Log the output if the
// command fails.
func (ips *IPSet) Run(args ...string) error {
	setName := args[0]
	ips.knownSets[setName] = struct{}{}

	optArgs := []string{"-!"}
	optArgs = append(optArgs, args...)
	cmd := exec.Command(ipsetBinary, optArgs...)
	var stderr bytes.Buffer
	cmd.Stderr = &stderr

	if err := cmd.Run(); err != nil {
		log.Printf("Error: command \"%s %s\" failed: %s\n%s",
			ipsetBinary, strings.Join(optArgs, " "), err, stderr.String())
		return err
	}

	return nil
}

// System state dump, for snapshots.
type ipsetDump struct {
	sets map[string][]byte `json:"sets"`
}

// Dump an ipset as a binary blob.
func (ips *IPSet) dumpSet(setName string) ([]byte, error) {
	cmd := exec.Command(ipsetBinary, "save", setName)
	var out bytes.Buffer
	cmd.Stdout = &out
	if err := cmd.Run(); err != nil {
		return nil, err
	}
	return out.Bytes(), nil
}

// Create a snapshot of the current ipset state.
func (ips *IPSet) Save() ([]byte, error) {
	dump := ipsetDump{
		sets: make(map[string][]byte),
	}
	var err error
	for setName, _ := range ips.knownSets {
		dump.sets[setName], err = ips.dumpSet(setName)
		if err != nil {
			return nil, err
		}
	}

	var b bytes.Buffer
	json.NewEncoder(&b).Encode(&dump)
	return b.Bytes(), nil
}

// Restore an ipset from a binary blob.
func (ips *IPSet) loadSet(setName string, entries []byte) error {
	cmd := exec.Command(ipsetBinary, "restore")
	cmd.Stdin = bytes.NewReader(entries)
	return cmd.Run()
}

// Restore ipsets from a snapshot.
func (ips *IPSet) Recovery(data []byte) error {
	var dump ipsetDump
	if err := json.NewDecoder(bytes.NewBuffer(data)).Decode(&dump); err != nil {
		return err
	}

	knownSets := make(map[string]struct{})
	for setName, entries := range dump.sets {
		knownSets[setName] = struct{}{}
		if err := ips.loadSet(setName, entries); err != nil {
			return err
		}
	}
	ips.knownSets = knownSets

	return nil
}

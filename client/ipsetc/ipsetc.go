package main

import (
	"flag"
	"log"
	"strings"

	"git.autistici.org/ale/ipsetd/client"
)

var (
	serverStr = flag.String("server", "localhost:1313", "Comma-separated list of servers")
	doCreate  = flag.Bool("create", false, "Create a new ipset")
	doAdd     = flag.Bool("add", false, "Add an entry to an ipset")
	doDel     = flag.Bool("del", false, "Delete an entry from an ipset")
)

func btou(b bool) int {
	if b {
		return 1
	}
	return 0
}

func main() {
	flag.Parse()

	numOpts := btou(*doCreate) + btou(*doAdd) + btou(*doDel)
	if numOpts != 1 {
		log.Fatal("Specify only one of --add, --del and --create")
	}
	if flag.NArg() < 2 {
		log.Fatal("Not enough arguments")
	}

	servers := strings.Split(*serverStr, ",")
	ipsetc := client.NewClient(servers)
	var err error
	if *doAdd {
		err = ipsetc.Add(flag.Arg(0), flag.Arg(1))
	} else if *doDel {
		err = ipsetc.Del(flag.Arg(0), flag.Arg(1))
	} else if *doCreate {
		var typeOptions []string
		if flag.NArg() > 2 {
			typeOptions = flag.Args()[2:]
		}
		err = ipsetc.Create(flag.Arg(0), flag.Arg(1), typeOptions)
	}
	if err != nil {
		log.Fatal(err)
	}
}

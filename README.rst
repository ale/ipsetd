++++++
ipsetd
++++++


``ipsetd`` maintains a consistent ipset_ across multiple servers. It
uses the RAFT_ protocol to replicate set membership between the
cluster nodes.

It can be useful to maintain responsive distributed blacklists, or
for automated anti-DDOS mechanisms.


Building
--------

Once you have your ``GOPATH`` set up, check out the source code in the
appropriate directory (in this case,
``$GOPATH/src/git.autistici.org/ale/ipsetd``) and build the
command-line programs::

    $ go get ./...
    $ go build ipsetd.go
    $ go build client/ipsetc/ipsetc.go

Install the binaries (``ipsetc`` and ``ipsetd``) somewhere you can
find them.


Usage
-----

Running the daemon
~~~~~~~~~~~~~~~~~~

Start the ``ipsetd`` daemon on each cluster node. To bootstrap the
cluster:

1. pick a machine and run ``ipsetd`` without the ``--peer`` option
2. on all other machines, start ``ipsetd`` once with the ``--peer``
   option pointing at the first machine

Once the cluster has been bootstrapped, there is no need to use the
``--peer`` option on further invocations, as the cluster membership is
encoded in the RAFT log.

You might want to limit access to the TCP port used by the daemon
(1313 is the default) using iptables, as no authentication is
currently provided by ``ipsetd``.


Managing replicated ipsets
~~~~~~~~~~~~~~~~~~~~~~~~~~

Use the ``ipsetc`` command to manipulate sets, as you would use the
standard ``ipset`` tool. The *add*, *del* and *create* commands are
supported, using a syntax similar to that of ``ipset``. For example::

    $ ipsetc --create blacklist hash:ip
    $ ipsetc --add blacklist 82.94.249.234

will create and populates an IP-based hash set called *blacklist*.


Caveats and TODOs
~~~~~~~~~~~~~~~~~

Note that ``ipsetd`` expects full control over the ipset that it
manages: if you perform local changes (perhaps using the standard
``ipset`` tool), they will not be replicated and will lead to
inconsistencies in the cluster.

Note also that, due to the log-based nature of the underlying RAFT
protocol, commands are not executed synchronously with requests, so
there is no way to retrieve or display their output or exit status.
This means that you should make sure that the ipset commands are
syntactically correct before invoking ``ipsetc``, or they will
silently fail when applied by the individual nodes in the cluster.

There is currently no way to remove nodes from the cluster (must
implement *LeaveCommand*).



.. _ipset: http://ipset.netfilter.org/
.. _RAFT: https://github.com/goraft/raft



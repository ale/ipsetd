package command

import (
	"log"

	"git.autistici.org/ale/ipsetd/ipset"
	"github.com/goraft/raft"
)

// Create a set.
type CreateCommand struct {
	SetName     string   `json:"set"`
	TypeName    string   `json:"type"`
	TypeOptions []string `json:"type_options"`
}

func NewCreateCommand(setName, typeName string, typeOptions []string) *CreateCommand {
	return &CreateCommand{
		SetName:     setName,
		TypeName:    typeName,
		TypeOptions: typeOptions,
	}
}

func (c *CreateCommand) CommandName() string {
	return "create"
}

func (c *CreateCommand) Apply(server raft.Server) (interface{}, error) {
	log.Printf("Apply(create, '%s', '%s', %+v)", c.SetName, c.TypeName, c.TypeOptions)
	ips := server.Context().(*ipset.IPSet)
	createArgs := []string{"create", c.SetName, c.TypeName}
	if c.TypeOptions != nil && len(c.TypeOptions) > 0 {
		createArgs = append(createArgs, c.TypeOptions...)
	}
	ips.Run(createArgs...)
	return nil, nil
}

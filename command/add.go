package command

import (
	"log"

	"git.autistici.org/ale/ipsetd/ipset"
	"github.com/goraft/raft"
)

// Adds an entry to a set.
type AddCommand struct {
	SetName string `json:"set"`
	Entry   string `json:"entry"`
}

func NewAddCommand(setName, entry string) *AddCommand {
	return &AddCommand{
		SetName: setName,
		Entry:   entry,
	}
}

func (c *AddCommand) CommandName() string {
	return "add"
}

func (c *AddCommand) Apply(server raft.Server) (interface{}, error) {
	log.Printf("Apply(add, '%s', '%s')", c.SetName, c.Entry)
	ips := server.Context().(*ipset.IPSet)
	ips.Run("add", c.SetName, c.Entry)
	return nil, nil
}

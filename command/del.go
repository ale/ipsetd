package command

import (
	"log"

	"git.autistici.org/ale/ipsetd/ipset"
	"github.com/goraft/raft"
)

// Delete an entry in a set.
type DelCommand struct {
	SetName string `json:"set"`
	Entry   string `json:"entry"`
}

func NewDelCommand(setName, entry string) *DelCommand {
	return &DelCommand{
		SetName: setName,
		Entry:   entry,
	}
}

func (c *DelCommand) CommandName() string {
	return "del"
}

func (c *DelCommand) Apply(server raft.Server) (interface{}, error) {
	log.Printf("Apply(del, '%s', '%s')", c.SetName, c.Entry)
	ips := server.Context().(*ipset.IPSet)
	ips.Run("del", c.SetName, c.Entry)
	return nil, nil
}

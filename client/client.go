package client

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net"
	"net/http"
	"net/url"
	"time"

	"git.autistici.org/ale/ipsetd/command"
)

// HTTP client with a short connect() timeout.
var httpClient *http.Client

func dialTimeout(network, addr string) (net.Conn, error) {
	return net.DialTimeout(network, addr, time.Duration(5*time.Second))
}

func init() {
	timeoutTransport := &http.Transport{
		Dial: dialTimeout,
	}
	httpClient = &http.Client{
		Transport: timeoutTransport,
	}
}

// Random, mutable iterator over a list of server endpoints, that
// visits every element exactly once.
type serverList struct {
	servers []string
	cur     string
	pos     int
}

// Advance the iterator.
func (l *serverList) Next() bool {
	if l.pos < len(l.servers) {
		l.cur = l.servers[l.pos]
		l.pos += 1
		return true
	}
	return false
}

// Return the current value.
func (l *serverList) Value() string {
	return l.cur
}

func (l *serverList) swap(i, j int) {
	l.servers[i], l.servers[j] = l.servers[j], l.servers[i]
}

// Push 'server' at the front of the list.
func (l *serverList) Push(server string) {
	// Find the index of 'server'.
	idx := -1
	for i, s := range l.servers {
		if s == server {
			idx = i
			break
		}
	}

	// Deal with the three possible cases: a new (previously
	// unknown) server, one that we've already tried, or one that
	// is present later in the list. Unless the server has already
	// failed, we will connect to it next.
	if idx < 0 {
		// Extend the array and swap the next item with the
		// new server.
		l.servers = append(l.servers, server)
		idx = len(l.servers) - 1
	} else if idx < l.pos {
		// Do nothing.
		return
	}

	// Swap the new server on the next spot in the list.
	l.swap(l.pos, idx)
}

func newServerList(servers []string) *serverList {
	l := &serverList{
		servers: make([]string, len(servers)),
	}

	// Permutate server list into the new array in random order.
	for idx, oldIdx := range rand.Perm(len(servers)) {
		l.servers[idx] = servers[oldIdx]
	}

	return l
}

// HTTP client that is slightly smarter than average. It will attempt
// to connect to multiple backends in sequence, and it will follow
// leader redirects where possible. This means that it should take at
// most 2 RPCs, after we find a node that is alive, to submit a write
// request to the current leader.
//
// If a server is specified using its DNS name, all the IP addresses
// associated with it will be resolved and considered as separate
// targets (all with the same port, clearly). This makes it possible
// to use a single DNS name for the entire cluster.
//
type SmartHTTPClient struct {
	servers []string
}

func NewSmartHTTPClient(servers []string) *SmartHTTPClient {
	// Resolve server names. Do it once per client, rather than on
	// every request (this reduces useless load on the local
	// resolver, assuming short-lived clients). DNS errors are not
	// fatal but cause the server to be ignored.
	resolved := make([]string, 0, len(servers))
	for _, server := range servers {
		host, port, err := net.SplitHostPort(server)
		if err != nil {
			log.Printf("Bad server spec '%s': %s", server, err)
			continue
		}

		addrs, err := net.LookupIP(host)
		if err != nil {
			log.Printf("Could not resolve '%s': %s", host, err)
			continue
		}

		for _, ip := range addrs {
			resolved = append(resolved, net.JoinHostPort(ip.String(), port))
		}
	}

	return &SmartHTTPClient{
		servers: resolved,
	}
}

func (c *SmartHTTPClient) Post(url string, contentType string, data io.Reader) error {
	return c.doRequest(func(server string) (*http.Response, error) {
		return httpClient.Post(fmt.Sprintf("http://%s%s", server, url), contentType, data)
	})
}

func (c *SmartHTTPClient) PostForm(url string, args url.Values) error {
	return c.doRequest(func(server string) (*http.Response, error) {
		return httpClient.PostForm(fmt.Sprintf("http://%s%s", server, url), args)
	})
}

// Attempt to send a request to all known targets, sequentially and in
// random order, until one replies successfully. Follow leader hints.
func (c *SmartHTTPClient) doRequest(fn func(string) (*http.Response, error)) error {
	outErr := errors.New("No peers found")

	// Iterate over servers.
	l := newServerList(c.servers)
	for l.Next() {
		server := l.Value()
		resp, err := fn(server)
		if err != nil {
			log.Printf("error for server %s: %s", server, err)
			outErr = err
			continue
		}
		resp.Body.Close()

		// If we receive a leader redirect, follow it by
		// bumping the leader in front of the remaining
		// servers list.
		if leader := resp.Header.Get("X-Leader"); resp.StatusCode == 409 && leader != "" {
			l.Push(leader)
		} else if resp.StatusCode != 200 {
			log.Printf("error for server %s: %s", server, resp.Status)
			outErr = errors.New(resp.Status)
		} else {
			return nil
		}
	}

	return outErr
}

// Client for the ipsetd HTTP interface.
type Client struct {
	*SmartHTTPClient
}

func NewClient(servers []string) *Client {
	return &Client{NewSmartHTTPClient(servers)}
}

func (c *Client) Create(setName, typeName string, typeOptions []string) error {
	cmd := command.NewCreateCommand(setName, typeName, typeOptions)
	var b bytes.Buffer
	if err := json.NewEncoder(&b).Encode(cmd); err != nil {
		return err
	}
	return c.Post("/api/1/ipset/create", "application/json", &b)
}

func (c *Client) Add(setName, entry string) error {
	cmd := command.NewAddCommand(setName, entry)
	var b bytes.Buffer
	if err := json.NewEncoder(&b).Encode(cmd); err != nil {
		return err
	}
	return c.Post("/api/1/ipset/add", "application/json", &b)
}

func (c *Client) Del(setName, entry string) error {
	cmd := command.NewDelCommand(setName, entry)
	var b bytes.Buffer
	if err := json.NewEncoder(&b).Encode(cmd); err != nil {
		return err
	}
	return c.Post("/api/1/ipset/del", "application/json", &b)
}

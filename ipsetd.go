package main

import (
	"flag"
	"log"
	"math/rand"
	"os"
	"time"

	"git.autistici.org/ale/ipsetd/command"
	"git.autistici.org/ale/ipsetd/server"
	"github.com/goraft/raft"
)

var (
	debug = flag.Bool("debug", false, "Show debugging messages")
	peer  = flag.String("peer", "", "host:port of a peer")
	path  = flag.String("path", "", "Log path")
	host  = flag.String("host", mustGetHostname(), "Name or IP of this host")
	port  = flag.Int("port", 1313, "Port to listen on")
)

func mustGetHostname() string {
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatal(err)
	}
	return hostname
}

func main() {
	flag.Parse()

	if *path == "" {
		log.Fatal("You must specify --path")
	}

	if *debug {
		raft.SetLogLevel(raft.Debug)
	}

	// Seed the global random generator.
	rand.Seed(time.Now().UnixNano())

	// Set up known commands.
	raft.RegisterCommand(&command.AddCommand{})
	raft.RegisterCommand(&command.DelCommand{})
	raft.RegisterCommand(&command.CreateCommand{})

	os.MkdirAll(*path, 0755)

	s := server.NewServer(*path, *host, *port)
	log.Fatal(s.ListenAndServe(*peer))
}
